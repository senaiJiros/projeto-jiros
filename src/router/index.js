import Vue from 'vue';
import Router from 'vue-router';
import { store } from '../store/store';
import Login from '@/components/Login';
import CadUsuario from '@/components/CadUsuario';
import CadDados from '@/components/administrador/CadastrarDados';
import ListCategorias from '@/components/administrador/ListCategoria';
import ListSubcategorias from '@/components/administrador/ListSubcategoria';
import ListClasses from '@/components/administrador/ListClasse';
import ListProdutos from '@/components/administrador/ListProduto';
<<<<<<< HEAD
import ListFabricantes from '@/components/administrador/ListFabricantes';
import ListUnidadesMedida from '@/components/administrador/ListUnidadesMedida';

=======
>>>>>>> 91a2b01316bf9f5a5576c169895df209a097810f
import EditCategoria from '@/components/administrador/EditCategoria';
import EditSubcategorias from '@/components/administrador/EditSubcategoria';
import EditClasses from '@/components/administrador/EditClasse';
import EditProdutos from '@/components/administrador/EditProduto';
import Home from '@/components/Home';
import SolicitaCota from '@/components/solicitante/SolicitaCota';
import ListProdutoSelecionados from '@/components/solicitante/ListProdutosSelecionados';
import SelFornecedor from '@/components/solicitante/SelFornecedor';
import ListFornecedorSelecionados from '@/components/solicitante/ListFornecedoresSelecionados';
import ConfCotacao from '@/components/solicitante/ConfCotacao';
import ListaSolicitacoes from '@/components/solicitante/ListaSolicitacoes';
import MapaComparativo from '@/components/solicitante/MapaComparativo';
import CotarSolicitacao from '@/components/fornecedor/CotarSolicitacao';



import Teste from '@/components/Teste';

Vue.use(Router);

let router = new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
    },
    {
      path: '/cadastrar/usuarios',
      name: 'CadUsuario',
      component: CadUsuario,
    },
    {
      path: '/cadastrar/dados',
      name: 'CadDados',
      component: CadDados,
    },
    {
      path: '/listar/categorias',
      name: 'ListCategoria',
      component: ListCategorias,
    },
    {
      path: '/listar/subcategorias',
      name: 'ListSubcategorias',
      component: ListSubcategorias
    },
    {
      path: '/listar/classes',
      name: 'ListClasse',
      component: ListClasses
    },
    {
      path: '/listar/produtos',
      name: 'ListProdutos',
      component: ListProdutos
    },
    {
<<<<<<< HEAD
      path: '/listar/fabricantes',
      name: 'ListFabricantes',
      component: ListFabricantes
    },
    {
      path: '/listar/unidadesMedida',
      name: 'ListUnidadesMedida',
      component: ListUnidadesMedida
    },
    {
=======
>>>>>>> 91a2b01316bf9f5a5576c169895df209a097810f
      path: '/editar/categorias',
      name: 'EditCategoria',
      component: EditCategoria,
    },
    {
      path: '/editar/subcategorias',
      name: 'EditSubcategorias',
      component: EditSubcategorias,
    },
    {
      path: '/editar/classes',
      name: 'EditClasses',
      component: EditClasses,
    },
    {
      path: '/editar/produtos',
      name: 'EditProdutos',
      component: EditProdutos,
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
    }, 
    {
      path: '/cotacoes/solicitar/solicitarCotacao',
      name: 'SolicitaCota',
      component: SolicitaCota,
    },
    {
      path: '/cotacoes/solicitar/listarProdutosSel',
      name: 'ListProdutoSelecionados',
      component: ListProdutoSelecionados,
    },
    {
      path: '/cotacoes/solicitar/selecionarFornecedor',
      name: 'SelFornecedor',
      component: SelFornecedor,
    },
    {
      path: '/cotacoes/solicitar/listarFornecedorSel',
      name: 'ListFornecedorSelecionados',
      component: ListFornecedorSelecionados,
    },
    {
      path: '/cotacoes/solicitar/confirmarSolicitacao',
      name: 'ConfCotacao',
      component: ConfCotacao,
    },
    {
      path: '/cotacoes/solicitar/mapaComparativo',
      name: 'MapaComparativo',
      component: MapaComparativo,
    },
    {
      path: 'cotacoes/visualizar/solicitacoes',
      name: 'ListaSolicitacoes',
      component: ListaSolicitacoes,
    },
    {
      path: '/cotacoes/realizar/cotarSolicitacao',
      name: 'CotarSolicitacao',
      component: CotarSolicitacao,
    },
<<<<<<< HEAD
    


=======
>>>>>>> 91a2b01316bf9f5a5576c169895df209a097810f
    {
      path: '/teste',
      name: 'Teste',
      component: Teste,
    },
  ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/')
  } else {
    next()
  }
})

export default router