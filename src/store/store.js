import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const servidor = 'http://192.168.4.54:8080/api/';

export const store = new Vuex.Store({
    state: {
        token: '',
        grupos: [],
        categorias: [],
        produtos: []
    },
    getters: {
        fetchToken: state => state.token,
        fetchGrupos: state => state.grupos,
        fetchCategorias: state => state.categorias,
        fetchProdutos: state => state.produtos,
        isLoggedIn: state => !!state.token,
    },
    mutations: {
        ADD_TOKEN: (state, payload) => {
            state.token = payload;
        },
        GET_GRUPOS: (state, payload) => {
            state.grupos = payload;
        },
        GET_CATEGORIAS: (state, payload) => {
            state.categorias = payload;
        },
        GET_PRODUTOS: (state, payload) => {
            state.produtos = payload;
        }
    },
    actions: {
        postToken: (context, payload) => {
            return axios({
                method: 'post',
                data: payload,
                url: servidor + 'auth/jwt',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((response) => {
                    context.commit("ADD_TOKEN", 'Bearer ' + response.data.token)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        // buscar todos os grupos
        fetchGrupos: (context, payload) => {
            return axios({
                method: 'get',
                url: servidor + 'groups',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                }
            })
                .then((response) => {
                    context.commit("GET_GRUPOS", response.data)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        // buscar todas as categorias
        fetchCategorias: (context, payload) => {
            return axios({
                method: 'get',
                url: servidor + 'categories',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                }
            })
                .then((response) => {
                    context.commit("GET_CATEGORIAS", response.data)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        fetchProdutos: (context, payload) => {
            return axios({
                method: 'get',
                url: servidor + 'products',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                }
            })
                .then((response) => {
                    context.commit("GET_PRODUTOS", response.data)
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        // cadastrar um novo grupo
        postGrupo: (context, payload) => {
            return axios({
                method: 'post',
                data: payload,
                url: servidor + 'groups',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                },
            })
                .then((response) => {
                    console.log(response.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        // fazer o cadastro de categoria
        postCategoria: (context, payload) => {
            return axios({
                method: 'post',
                data: payload,
                url: servidor + 'categories',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                },
            })
                .then((response) => {
                    console.log(response.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        postProduto: (context, payload) => {
            return axios({
                method: 'post',
                data: payload,
                url: servidor + 'products',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': store.state.token
                },
            })
                .then((response) => {
                    console.log(response.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
    }
})